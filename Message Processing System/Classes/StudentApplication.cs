﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace Message_Processing_System
{
    public class StudentApplication
    {
        private string _identifier;
        private string _message;
        private string _entryLevel;
        private List<string> _subjects;
        private List<string> _universities;

        public string identifier
        {
            get { return _identifier; }
            set 
            {
                //if (value == "" || value == " ")
                //    throw new ArgumentException("Applicant name field may not be blank.");
                _identifier = value; 
            }
        }

        public string message
        {
            get { return _message; }
            set { _message = value; }
        }

        public string entryLevel
        {
            get { return _entryLevel; }
            set { _entryLevel = value; }
        }

        public List<string> subjects
        {
            get { return _subjects; }
            set { _subjects = value; }
        }

        public List<string> universities
        {
            get { return _universities; }
            set { _universities = value; }
        }

        // Constructor
        public StudentApplication()
        {
            // Instantiate lists
            _subjects = new List<string>();
            _universities = new List<string>();
        }

        public StudentApplication(string identifier, string message, string entryLevel, List<string> subjects, List<string> universities)
        {
            // Instantiate lists
            _subjects = new List<string>();
            _universities = new List<string>();

            // Assign class attribs from parameters
            _identifier = identifier;
            _message = message;
            _entryLevel = entryLevel;
            _subjects = subjects;
            _universities = universities;

        }

        public void WriteJson(string filePath)
        {
            List<StudentApplication> _data = new List<StudentApplication>();
            _data.Add(new StudentApplication()
            {
                identifier = _identifier,
                message = _message,
                entryLevel = _entryLevel,
                subjects = _subjects,
                universities = _universities
            });
            // Serialize data
            string json = JsonConvert.SerializeObject(_data.ToArray());
            // Append json data to file
            System.IO.File.AppendAllText(filePath, json);
        }

        // Checks if the given string refers to a student entry level (Undergraduate or Postgraduate)
        public static string GetEntryLevel(string text)
        {
            List<string> ugList = new List<string>(new string[] { "Undergraduate", "UG", "U/G" });
            List<string> pgList = new List<string>(new string[] { "Postgraduate", "PG", "P/G" });

            // Check for undergraduate match
            foreach (string word in ugList)
            {
                if (text.IndexOf(word, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return "Undergraduate";
                }
            }
            // Check for postgraduate match
            foreach (string word in pgList)
            {
                if (text.IndexOf(word, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return "Postgraduate";
                }
            }

            return "No match";
        }

        public static bool isBlank(string value)
        {
            if (value == "" || value == " ")
            {
                return true;
            }
            else
            { 
                return false;
            }
        }

    }
}
