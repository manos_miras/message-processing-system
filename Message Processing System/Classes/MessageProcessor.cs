﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;

namespace Message_Processing_System
{
    public class MessageProcessor
    {

        public const string uniPath = "../../Resources/University List.csv"; // Path for university csv file
        public const string wordsPath = "../../Resources/textwords.csv"; // Path for illegal words csv file
        public const string subjectsPath = "../../Resources/Subject List.csv"; // Path for subjects csv file
        public const string validMessagePath = "../../Resources/Applications/valid.txt"; // Path for valid messages
        public const string quarantineMessagePath = "../../Resources/Applications/quarantine.txt"; // Path for quarantined messages
        public const string applicationsPath = "../../Resources/Applications/applications.json"; // Path for JSON applications

        // Populates a list from a file, with a given path
        public void PopulateList(string path, List<string> list)
        {
            using (StreamReader sr = new StreamReader(path))
            {
                string currentLine;
                // currentLine will be null when the StreamReader reaches the end of file
                while ((currentLine = sr.ReadLine()) != null)
                {
                    List<string> Seperated = new List<string>();
                    if (currentLine.Contains(','))
                    {
                        Seperated = currentLine.Split(',').ToList<string>();
                        foreach (string seperateWord in Seperated)
                        {
                            if (seperateWord != "")
                            {
                                list.Add(seperateWord);
                            }
                        }
                    }
                    else
                    {
                        list.Add(currentLine);
                    }
                }
            }
        }

        // Checks some text for profanity / illegal words
        public bool IllegalWordCheck(string text, List<string> list)
        {
            // Check for match
            foreach (string word in list)
            {
                if (text.IndexOf(word, StringComparison.OrdinalIgnoreCase) >= 0) // IndexOf(word, StringComparison.OrdinalIgnoreCase) >= 0) // problem: TA matches with bastard
                {
                    var regex1 = new Regex(@"\W" + word + @"\W");
                    var regex2 = new Regex(word + @"\W");
                    var regex3 = new Regex(@"\W" + word);

                    //MessageBox.Show(word + ", 1 is match: " + regex1.IsMatch(text) + ", 2 is match: " + regex2.IsMatch(text) + ", 3 is match: " + regex3.IsMatch(text) + ", equals:" + text.Equals(word));
                    if (text.Equals(word, StringComparison.OrdinalIgnoreCase) || regex1.IsMatch(text) || regex2.IsMatch(text) || regex3.IsMatch(text))
                    {
                        MessageBox.Show(word);
                        return true; // Match
                    }
                }
            }
            return false; // No match
        }

        // Checks some text to see if it contains strings from a given list. Returns a new list with the occurences.
        public List<string> GetWordMatch(string text, List<string> list)
        {
            List<string> newList = new List<string>();
            // Check for match
            foreach (string word in list)
            {
                if (text.IndexOf(word, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    //MessageBox.Show(word + " " + list.Count);
                    newList.Add(word);
                }
            }

            if (newList.Count == 0)
            {
                newList.Add("No match.");
            }

            return newList;
        }

        // Appends a line of text to a file
        public void WriteToFile(string pathName, string message)
        {

            using (FileStream fs = new FileStream(pathName, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine(message);
            }

        }

        // Reads the last line of text from the file (latest)
        public string ReadFromFile(string pathName)
        {
            return File.ReadLines(pathName).Last();
        }

        public static StudentApplication FilterAndProcess(string name, string message)
        {
            List<string> universities = new List<string>(); // List of universities
            List<string> words = new List<string>(); // List of illegal words
            List<string> subjects = new List<string>(); // List of subjects

            MessageProcessor mesP = new MessageProcessor();

            // Populate lists
            mesP.PopulateList(MessageProcessor.uniPath, universities);
            mesP.PopulateList(MessageProcessor.wordsPath, words);
            mesP.PopulateList(MessageProcessor.subjectsPath, subjects);

            // Compare message to lists
            if (mesP.IllegalWordCheck(message, words))
            {
                mesP.WriteToFile(MessageProcessor.quarantineMessagePath, message);
                return null;
            }
            else
            {
                mesP.WriteToFile(MessageProcessor.validMessagePath, message);
            }

            // Update message value from valid messages
            message = mesP.ReadFromFile(MessageProcessor.validMessagePath);

            List<string> detectedUniversities = new List<string>();
            List<string> detectedSubjects = new List<string>();

            detectedUniversities = mesP.GetWordMatch(message, universities);
            detectedSubjects = mesP.GetWordMatch(message, subjects);

            string entryLevel = StudentApplication.GetEntryLevel(message);

            StudentApplication studentApp = new StudentApplication(name, message, entryLevel, detectedSubjects, detectedUniversities);

            // Append application to the applications file in Json format
            studentApp.WriteJson(MessageProcessor.applicationsPath);

            return studentApp;
        }
    }
}
