﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace Message_Processing_System
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // Event handler for Submit button
        private void submit_btn_Click(object sender, RoutedEventArgs e)
        {
            if (StudentApplication.isBlank(input_txtbox.Text))
            {
                MessageBox.Show("The input textbox is empty, a message is required.");
            }

            else if (StudentApplication.isBlank(name_txtbox.Text))
            {
                MessageBox.Show("The name textbox is empty, a name is required.");
            }
            else
            {
                clearOutputFields();

                StudentApplication studentApp = new StudentApplication();

                studentApp = MessageProcessor.FilterAndProcess(name_txtbox.Text, input_txtbox.Text);

                if (studentApp != null)
                {
                    // Show unis
                    displayListOnListbox(studentApp.universities, universities_lst);

                    // Show subjects
                    displayListOnListbox(studentApp.subjects, subjects_lst);

                    // Show entry level
                    entryLevel_txtbox.Text = studentApp.entryLevel;
                }
                else
                {
                    //MessageBox.Show("Message added to quarantine file, no further processing.");
                }
            }
        }

        private void displayListOnListbox(List<string> list, ListBox listBox)
        {
            foreach (string text in list)
            {
                listBox.Items.Add(text);
            }
        }

        private void clear_btn_Click(object sender, RoutedEventArgs e)
        {
            name_txtbox.Clear();
            input_txtbox.Clear();
            entryLevel_txtbox.Clear();
            subjects_lst.Items.Clear();
            universities_lst.Items.Clear();
        }

        private void clearOutputFields()
        {
            entryLevel_txtbox.Clear();
            subjects_lst.Items.Clear();
            universities_lst.Items.Clear();
            entryLevel_txtbox_2.Clear();
            subjects_lst_2.Items.Clear();
            universities_lst_2.Items.Clear();
        }

        private void browse_btn_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".txt";
            dlg.Filter = "TXT Files (*.txt)|*.txt";


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                path_txtbox.Text = filename;
            }
        }

        private void submit_import_btn_Click(object sender, RoutedEventArgs e)
        {

            string messagePath = path_txtbox.Text;

            string message = File.ReadAllText(messagePath);

            if (StudentApplication.isBlank(message))
            {
                MessageBox.Show("The specified file is empty, a message is required.");
            }

            else if (StudentApplication.isBlank(name_txtbox_2.Text))
            {
                MessageBox.Show("The name textbox is empty, a name is required.");
            }
            else
            {

                clearOutputFields();

                StudentApplication studentApp = new StudentApplication();

                studentApp = MessageProcessor.FilterAndProcess(name_txtbox_2.Text, message);

                if (studentApp != null)
                {

                    // Show unis
                    displayListOnListbox(studentApp.universities, universities_lst_2);

                    // Show subjects
                    displayListOnListbox(studentApp.subjects, subjects_lst_2);

                    // Show entry level
                    entryLevel_txtbox_2.Text = studentApp.entryLevel;
                }
                else
                {
                    //MessageBox.Show("Message added to quarantine file, no further processing");
                }
            }
        }
    }
}
